# Node-React Tutorial

Looking to upskill for some development projects, 
I started by looking for some Medium tutorials. 
A nice one I found was:
* [Let’s Build: Cryptocurrency Native Mobile App With React Native + Redux](https://medium.com/react-native-training/tutorial-react-native-redux-native-mobile-app-for-tracking-cryptocurrency-bitcoin-litecoin-810850cf8acc)
* [Let’s Build: Cryptocurrency Native Mobile App With React Native + Redux — Chapter II](https://medium.com/react-native-training/tutorial-react-native-redux-native-mobile-app-for-tracking-cryptocurrency-bitcoin-litecoin-810850cf8acc)
* [Let’s Build: Cryptocurrency Native Mobile App With React Native + Redux — Chapter III](https://medium.com/react-native-training/learn-how-to-build-a-rn-redux-cryptocurrency-app-chapter-iii-a454dda156b)
* [Let’s Build: Cryptocurrency Native Mobile App With React Native + Redux — Chapter IV](https://codeburst.io/learn-how-to-build-a-rn-redux-cryptocurrency-app-chapter-iv-b0e0c5ca2dca)
